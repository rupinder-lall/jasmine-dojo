# Quality Assaurance with Jasmine
***
## What is Jasmine?
- Jasmine is a behavior-driven development framework for testing javascript code.
- It does not rely on browsers, DOM, or any JavaScript framework. 
- Its suitable for websites, Node.js projects or anywhere that Javascript can run.
- It has a clean obvious syntax so that you can easily write tests.
***
## Add Jasmine to your project
#### Testing with jasmine in the browser
Download the jasmine standalone zip file from [here](https://github.com/jasmine/jasmine/releases?source=post_page---------------------------).

Unzip the folder and move it into the root of your project folder. 
#### Testing with jasmine on the server (node)

```
// create a nodejs project
npm init

// install dependencies for jasmine and console reporter
npm install jasmine --save
npm install jasmine-console reporter --save-dev

```
***
## Syntax
#### Describe
- Is part of a suite block
- Contains two parameters, the name of the describe block and another being the function declaration
```
describe(description ‘Calculator’, specDefinitions: function(){

})
```
#### It
- Goes within a describe block
- Also known as a spec
- This is the block which actually contains each unit test case
```
it(‘Should add two numbers’, function(){
	
})
```
***
## Inbuilt Matchers
The matchers which are inbuilt in the Jasmine framework are called inbuilt matchers.

+ expect(true).toBe(true)
+ expect(true).not.toBe(true)
+ expect(a).toEqual(bar)
+ expect(message).toMatch(/bar/)
+ expect(message).toMatch('bar')
+ expect(a.foo).toBeDefined()
+ expect(a.foo).toBeUndefined()
+ expect(a.foo).toBeNull()
+ expect(a.foo).toBeTruthy()
+ expect(a.foo).toBeFalsy()
+ expect(message).toContain('hello')
+ expect(pi).toBeGreaterThan(3)
+ expect(pi).toBeLessThan(4)
+ expect(pi).toBeCloseTo(3.1415, 0.1)

***
## Custom Matchers
```
describe('Person', function () {
   var person;

   beforeEach(function () {
       person = new Person();
      
       jasmine.addMatchers({
           // we should add custom matchers in beforeEach function()
           validateAge: function () {
               //name of the custom matcher
               return {
                   compare: function (actual, expected) {
                       // compare function that takes an actual value and expected value
                       // expect(10).matcher(20);   actual will be 10 and expected will be 20
                       var result = {};
                       result.pass = (actual >= 18);
                       // our custom logic goes here
                       result.message = 'sorry you are under age';
                       // this message will be shown when the test has failed
                       return result;
                   }
               }
           }
       })
   })


   it('should be legal to drink', function () {
       var yourAge = person.age();
     //  var yourAge = 17;
       expect(yourAge).validateAge();
   });
})
```