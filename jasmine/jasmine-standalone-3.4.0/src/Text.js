var Hover = (function () {

    function p(id, innerText, fontSize, color, clickedColor) {
        var text = document.createElement("p");
        text.id = id;
        text.innerText = innerText;
        text.style.fontSize = fontSize;
        text.style.color = color;

        text.addEventListener("click", function () {
            var boxClicked = text.style.color = clickedColor;
        });

        return text;
    }

    return {
        p: p
    }
})